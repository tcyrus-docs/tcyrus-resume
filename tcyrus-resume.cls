% !TeX root = resume.tex

% Intro Options
\ProvidesClass{tcyrus-resume}[CV class]
\NeedsTeXFormat{LaTeX2e}
\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{
  \PassOptionsToClass{\CurrentOption}{article}
}
\ProcessOptions\relax
\LoadClass{article}

% Package Imports
\usepackage[hidelinks]{hyperref}
\usepackage[hmargin=1.0cm, vmargin=0.75cm]{geometry}

% Publications
\usepackage{cite}
\renewcommand\refname{\vskip -1.5cm}

% Color definitions
\usepackage[usenames, dvipsnames]{xcolor}
\definecolor{date}{gray}{0.4}
\definecolor{primary}{gray}{0.17}
\definecolor{headings}{gray}{0.42}
\definecolor{subheadings}{gray}{0.2}

% Set main fonts
\usepackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
\usepackage{sourcesanspro}
\setmainfont[
	Color=primary,
	ItalicFont=SourceSansPro-LightIt,
	BoldFont=SourceSansPro-Regular,
	BoldItalicFont=SourceSansPro-RegularIt
]{SourceSansPro-Light}

% Name command
\newfontfamily\infofont[Color=headings]{SourceSansPro-Regular}
\newcommand{\namesection}[3]{
	\centering{
		\fontsize{25pt}{45pt}
		{\fontspec{SourceSansPro-ExtraLight}\selectfont #1 }
		{\fontspec{SourceSansPro-Light}\selectfont #2}
	} \\[5pt]
	\centering{
		\infofont\fontsize{10pt}{13pt}\selectfont #3
	} \\[5pt]
	\noindent\makebox[\linewidth]{\color{headings}\rule{\paperwidth}{0.5pt}}
	\vspace{-10pt}
}

% Section separators
\usepackage{titlesec}
\titlespacing*{\section}{0pt}{7pt}{0pt}
\titlespacing*{\subsection}{0pt}{7pt}{0pt}
\newcommand{\sectionsep}{\vspace{10pt}}

% Headings command
\newfontfamily\sectionheadingfont[Color=headings,Letters=SmallCaps]{SourceSansPro-Light}
\titleformat{\section}{
	\sectionheadingfont\fontsize{15pt}{23pt}\raggedright\selectfont
}{}{0em}{}

% Subheadings command
\newfontfamily\subsectionheadingfont[Color=subheadings,Letters=SmallCaps]{SourceSansPro-Regular}
\titleformat{\subsection}{
	\subsectionheadingfont\fontsize{12pt}{12pt}\raggedright\selectfont\bfseries
}{}{0em}{}

\newfontfamily\runsubsectionfont[Color=subheadings,Letters=SmallCaps]{SourceSansPro-Semibold}
\newcommand{\runsubsection}[1]{
	\runsubsectionfont\fontsize{12pt}{13pt}\selectfont {#1} \normalfont
}

% Descriptors command
\newfontfamily\descriptfont[Color=subheadings,Letters=SmallCaps]{SourceSansPro-Regular}
\newcommand{\descript}[1]{
	\descriptfont\fontsize{11pt}{13pt}\raggedright\selectfont {#1\\} \normalfont
}

% Location command
\newfontfamily\locationfont[Color=headings]{SourceSansPro-Regular}
\newcommand{\location}[1]{
	\locationfont\fontsize{11pt}{12pt}\raggedright\selectfont {#1\\} \normalfont
}

% Bullet Lists with fewer gaps command
\usepackage[inline]{enumitem}

\newlist{tightemize}{itemize}{1}
\setlist[tightemize]{
	nosep,
	label=\textbullet,
	leftmargin=*,
	itemsep=1pt,
	parsep=0pt
}

\newlist{skillemize}{itemize*}{1}
\setlist[skillemize]{
	label={},
	itemjoin={~\textbullet\ },
	after=\vspace{2pt},
	afterlabel={}
}
